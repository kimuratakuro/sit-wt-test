@echo off

set RESULT1=
for /f "usebackq" %%a in (`findstr """"activationStatus""":"""%3"""" C:\Users\tie505209\Documents\sit-wt-test\workspace\default\evidence\TMPLogs\%1\jcb-tsp-es.StatusNotification_%2.log ^| find /c /v ""`) do set RESULT1=%%a

set RESULT2=
for /f "usebackq" %%a in (`findstr """"deploymentStatus""":"""%4"""" C:\Users\tie505209\Documents\sit-wt-test\workspace\default\evidence\TMPLogs\%1\jcb-tsp-es.StatusNotification_%2.log ^| find /c /v ""`) do set RESULT2=%%a

set RESULT3=
for /f "usebackq" %%a in (`findstr """"suspensionStatus""":"""%5"""" C:\Users\tie505209\Documents\sit-wt-test\workspace\default\evidence\TMPLogs\%1\jcb-tsp-es.StatusNotification_%2.log ^| find /c /v ""`) do set RESULT3=%%a

set /A RESULT=%RESULT1%+%RESULT2%+%RESULT3%

echo %RESULT%

call :SuccessOrDie
:SuccessOrDie
if %RESULT% == 6 (
    exit 0
)
exit /b 1